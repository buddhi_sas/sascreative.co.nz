<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sas_Creative
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</aside><!-- #secondary -->


add_action('woocommerce_after_shop_loop_item', 'add_a_custom_button', 5 );
function add_a_custom_button() {
    global $product;

    // Not for variable and grouped products that doesn't have an "add to cart" button
    if( $product->is_type('variable') || $product->is_type('grouped') ) return;

    // Output the custom button linked to the product
    echo '<span class="add-to-cart" data-title="Add to cart">
        <a class="button product_type_simple add_to_cart_button ajax_add_to_cart custom-button" href="' . esc_attr( $product->get_permalink() ) . '">' . __('View product') . '</a>
    </span>';
}


.header.style-05_2 .box-nav-vertical .block-title, .header.style-05_2~.header-sticky .box-nav-vertical .block-title {
    font-size: 14px;
    line-height: 30px;
    padding: 10px 38px;
}

.logo img {
    max-width: 100%;
    padding: 6px;
    max-height: 70px;
}

.header.style-05_2 .header-mid .header-inner {
    padding: 0px 0;
}

.header.style-05_2 .box-header-nav .main-menu>.menu-item>a, .header.style-05_2~.header-sticky .box-header-nav .main-menu>.menu-item>a {
    font-size: 14px;
    font-weight: 600;
    padding: 15px 0 10px;
}

@media (min-width: 1500px) {
.header.style-05_2 .box-header-nav .main-menu>.menu-item, .header.style-05_2 .header-control .inner-control>*, .header.style-06 .box-header-nav .main-menu>.menu-item, .header.style-07 .box-header-nav .main-menu>.menu-item, .header.style-09 .box-header-nav .main-menu>.menu-item {
    margin: 0 12px;
}
}

.header.style-05_2 .header-bot, .header.style-05_2~.header-sticky {
    background: rgb(2,28,122);
background: linear-gradient(90deg, rgba(2,28,122,1) 0%, rgba(5,116,229,1) 100%);
}

.text_main_color_2, a.backtotop:hover, a.backtotop:focus, .elementor-button:focus, .elementor-button:hover, .elementor-button:visited, .elementor-button, .added_to_cart, .button, button, input[type="button"], input[type="submit"], .added_to_cart:hover, .added_to_cart:focus, .button:hover, .button:focus, button:hover, input[type="button"]:hover, input[type="submit"]:hover, div.dgwt-wcas-search-wrapp button.dgwt-wcas-search-submit, .header-control .inner-control > .block-userlink > a .icon, .header-control .inner-control > .block-minicart > a .count, .ovic-socials a:hover, .ovic-slide.style-03 .slide-item .link:hover, .ovic-title.style-01 .title, .header.style-02 .logo .logo-text, .header.style-02 ~ .header-sticky .header-control .inner-control > * > a, .header.style-02 .header-control .inner-control > * > a, .entry-summary a.compare:hover, .entry-summary .yith-wcwl-add-to-wishlist > *:hover, .entry-summary .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse, .entry-summary .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse, .product-item.list a.compare:hover, .product-item.list a.yith-wcqv-button:hover, .product-item.list .yith-wcwl-add-to-wishlist > *:hover, .product-item.list .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse, .product-item.list .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse, .product-item.style-05 a.compare:hover, .product-item.style-05 a.yith-wcqv-button:hover, .product-item.style-05 .yith-wcwl-add-to-wishlist > *:hover, .product-item.style-05 .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse, .product-item.style-05 .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse, .product-item.style-12 a.yith-wcqv-button:hover, .product-item.style-12 .yith-wcwl-add-to-wishlist > *:hover, .product-item.style-12 .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse, .product-item.style-12 .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse, .product-item.style-12 .add-to-cart a, .product-item.style-13 a.yith-wcqv-button:hover, .product-item.style-13 .yith-wcwl-add-to-wishlist > *:hover, .product-item.style-13 .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse, .product-item.style-13 .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse, .product-item.style-14 a.yith-wcqv-button:hover, .product-item.style-14 .yith-wcwl-add-to-wishlist > *:hover, .product-item.style-14 .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse, .product-item.style-14 .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse, .product-item.style-14 .add-to-cart a, .product-item.style-15 a.yith-wcqv-button:hover, .product-item.style-15 .yith-wcwl-add-to-wishlist > *:hover, .product-item.style-15 .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse, .product-item.style-15 .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse, .product-item.style-15 .add-to-cart a, .product-item.style-18 a.yith-wcqv-button:hover, .product-item.style-18 .yith-wcwl-add-to-wishlist > *:hover, .product-item.style-18 .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse, .product-item.style-18 .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse, .product-item.style-19 a.yith-wcqv-button:hover, .product-item.style-19 .yith-wcwl-add-to-wishlist > *:hover, .product-item.style-19 .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse, .product-item.style-19 .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse, .product-item.style-19 .add-to-cart a, .product-item.style-20 a.yith-wcqv-button:hover, .product-item.style-20 .yith-wcwl-add-to-wishlist > *:hover, .product-item.style-20 .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse, .product-item.style-20 .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse, .product-item.style-20 .add-to-cart a, .product-item.style-21 a.yith-wcqv-button:hover, .product-item.style-21 .yith-wcwl-add-to-wishlist > *:hover, .product-item.style-21 .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse, .product-item.style-21 .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse, .product-item.style-21 .add-to-cart a, .product-item.style-22 a.yith-wcqv-button:hover, .product-item.style-22 .yith-wcwl-add-to-wishlist > *:hover, .product-item.style-22 .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse, .product-item.style-22 .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse, .product-item.style-22 .add-to-cart a, .product-item.style-24 a.yith-wcqv-button:hover, .product-item.style-24 .yith-wcwl-add-to-wishlist > *:hover, .product-item.style-24 .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse, .product-item.style-24 .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse, .product-item.style-25 a.yith-wcqv-button:hover, .product-item.style-25 .yith-wcwl-add-to-wishlist > *:hover, .product-item.style-25 .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse, .product-item.style-25 .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse, .product-item.style-25 .add-to-cart a, .product-item.style-26 a.yith-wcqv-button:hover, .product-item.style-26 .yith-wcwl-add-to-wishlist > *:hover, .product-item.style-26 .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse, .product-item.style-26 .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse, .product-item.style-26 .add-to-cart a, .post-item.style-01 .cat-list, .single-post > .cat-list, .woocommerce-pagination ul li a:hover, .woocommerce-pagination ul li .current, .tagcloud a:hover, .pagination-thumb .link:hover .icon, .header.style-05 ~ .header-sticky .header-control .inner-control > * > a .count, .header.style-05 .header-control .inner-control > * > a .count, .header.style-05_2 ~ .header-sticky .header-control .inner-control > * > a .count, .header.style-05_2 .header-control .inner-control > * > a .count, .header.style-06 ~ .header-sticky .header-control .inner-control > * > a .count, .header.style-06 .header-control .inner-control > * > a .count, .header.style-07 ~ .header-sticky .header-control .inner-control > * > a .count, .header.style-07 .header-control .inner-control > * > a .count, .header.style-07_2 ~ .header-sticky .header-control .inner-control > * > a .count, .header.style-07_2 .header-control .inner-control > * > a .count, .header.style-08 ~ .header-sticky .header-control .inner-control > * > a .count, .header.style-08 .header-control .inner-control > * > a .count, .header.style-09 ~ .header-sticky .header-control .inner-control > * > a .count, .header.style-09 .header-control .inner-control > * > a .count, .header.style-12 ~ .header-sticky .header-control .inner-control > * > a .count, .header.style-12 .header-control .inner-control > * > a .count, .header.style-15 ~ .header-sticky .header-control .inner-control > * > a .count, .header.style-15 .header-control .inner-control > * > a .count, .header.style-16 ~ .header-sticky .header-control .inner-control > * > a .count, .header.style-16 .header-control .inner-control > * > a .count, .header.style-17 ~ .header-sticky .header-control .inner-control > * > a .count, .header.style-17 .header-control .inner-control > * > a .count, .ovic-tab.style-02 .tab-title, .ovic-countdown .price-countdown, .ovic-banner.style-31 .text-01, .ovic-slide.style-05 .slide-item .link:hover, .contact-icon .elementor-icon, .ovic-tab.style-04 .tab-item.active .tab-link, .ovic-tab.style-04 .tab-item .tab-link:hover, .header.style-11 ~ .header-sticky .header-control .inner-control > * > a .count, .header.style-11 .header-control .inner-control > * > a .count, .header.style-14 ~ .header-sticky .header-control .inner-control > * > a .count, .header.style-14 .header-control .inner-control > * > a .count, .header.style-14 ~ .header-sticky .box-nav-vertical .block-title, .header.style-14 .box-nav-vertical .block-title, .ovic-category.style-02 .content, .ovic-tab.style-05 .tab-item.active .tab-link, .ovic-tab.style-05 .tab-item .tab-link:hover, .blog-item.style-06 .post-date, .header.style-07 .header-top, .product-item.style-16 .armania-countdown-wrapper, .ovic-category.style-03 .link:hover, .blog-item.style-07 .post-date, .ovic-tab.style-09 .tab-item.active .tab-link, .ovic-tab.style-09 .tab-item .tab-link:hover, .blog-item.style-10 .cat-list, .header.style-12 .header-message, .header.style-12 .block-search, .ovic-countdown.style-04 .subtitle, .ovic-newsletter.style-11 .icon, .ovic-newsletter.style-11 .head, .ovic-newsletter.style-12 .icon, .ovic-newsletter.style-12 .head, .header.style-12 div.dgwt-wcas-search-wrapp .dgwt-wcas-preloader, .header.style-05_2 ~ .header-sticky .box-nav-vertical .block-title, .header.style-05_2 .box-nav-vertical .block-title, .ovic-tab.style-11 .tab-item.active .tab-link, .ovic-tab.style-11 .tab-item .tab-link:hover, .ovic-tab.style-12 .tab-item.active .tab-link, .ovic-tab.style-12 .tab-item .tab-link:hover, .ovic-deal.style-05 .armania-countdown-wrapper, .ovic-social-network.style-02 .title, .skin-furniture-2 .product-labels > .onsale, .header.style-16_3 .header-message, .modal-demo-menu .demo.new a::after, .menu-item.demo .megamenu .new .elementor-image a::after, .skin-carparts .product-labels > .onsale, .skin-furniture .product-labels > .onsale, .skin-medical .product-labels > .onnew, .skin-tools .product-labels > .onsale, .skin-plant .product-labels > .onsale {
	color:#141414!important;
}

.header.style-05_2 .header-info {
    color: #fff;
}

div.dgwt-wcas-search-wrapp button.dgwt-wcas-search-submit {
	    background: transparent!important;
}

a:focus, a:hover {
    color: #23527c;
    text-decoration: none!important;
}

.product-item.style-02 .product-thumb {
    width: 100%;
        max-width: 100%;
    direction: ltr;
}

.product-item.style-02 .product-info {
    flex: 1 1 auto;
    width: 100%;
    padding: 13px 15px 0 0;
    direction: ltr;
}

.product-item.style-02 .product-inner {
    display: inline-block;
}

/* .product-inner {
	border-color: #e6e6e6;
	border-left: 1px solid transparent;
} */


.product-item .product-title {
    font-size: 28px;
    line-height: 34px;
    font-weight: 600;
    margin: 0;
}


.yith-wcwl-add-button {
    display: none;
}

.woocommerce-product-details__short-description li {
    line-height: 26px;
}

.product-item .woocommerce-product-details__short-description {
    max-height: 55px;
}


.product-item .price {
    margin-top: 6px;
    color: #292929;
}



.product-item.style-02 .group-button {
	max-width: 100%;
      text-align: center;
    font-size: 0;
    line-height: 0;
    letter-spacing: 0;
    margin-top: 10px;
    margin-bottom: 17px;
    display: flex;
}

.product-item.style-02 .add-to-cart {
    display: block;
    margin: 6px;
    width: 50%;
}

.product-item.style-21 .product-title {
    font-size: 22px;
    line-height: 20px;
    margin-top: 4px;
}

/* .button {
    display: inline-block;
    vertical-align: middle;
    padding: 13px 15px;
    margin: 13px 0 0;
    width: 240px;
    max-width: 100%;
} */


.woocommerce.single-product .product .yith-ywraq-add-to-quote {
    display: inline-block;
    line-height: normal;
    vertical-align: middle;
    width: 100%;
    margin: 30px 0;
}

.single-product .entry-summary .product_title {
    font-size: 40px;
    line-height: 24px;
    margin: 6px 0 0;
}

/* 
.block-wishlist.block-woo {
    display: none;
} */


h2.product-title a:hover, h2.product-title a:focus {
    color: #023396!important;
}

.contact-icon .elementor-icon {
    width: 60px!important;
    height: 60px!important;
    display: flex;
    align-items: center;
    justify-content: center;
    margin: auto;
    border-radius: 50%;
}

.site-content {
    padding-bottom: 0;
}

.woocommerce {
    margin-bottom: 4%!important;
}

.product-item.style-02 .product-info {
    flex: 1 1 auto;
    width: 100%;
    padding: 0;
    direction: ltr;
}

a.button.product_type_simple.add_to_cart_button.ajax_add_to_cart.custom-button {
    background: #fff;
}
.product-info.equal-elem {
	height: 202px!important;
}
a.button.product_type_simple.add_to_cart_button.ajax_add_to_cart.custom-button:hover {
    background: #efefef;
}

@media (min-width: 768px)
{
.header-top .header-inner>* {

    float: right;
}
}

.post-meta {
    display: none;
}
a.post-date {
    font-size: 14px;
    margin-top: 10px;
    display: block;
}