<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sas_Creative
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body class="bg-black">

	<header class="lg:px-16 px-8  flex flex-wrap items-center py-4 shadow-md">
  <div class="container mx-auto px-4">
    <div class="flex-1 flex justify-between items-center">
      <a href="#" class="text-xl">
	  
	  </a>
    </div>

    <label for="menu-toggle" class="pointer-cursor md:hidden block">
      <svg class="fill-current text-gray-900"
        xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
        <title>menu</title>
        <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path>
      </svg>
    </label>
    <input class="hidden" type="checkbox" id="menu-toggle" />

    <div class="hidden md:flex md:items-center md:w-auto w-full" id="menu">
      <nav>
        <ul class="md:flex items-center justify-between text-base text-white pt-4 md:pt-0">
          <li><a class="md:p-4 py-3 px-0 block" href="#">AboutUs</a></li>
          <li><a class="md:p-4 py-3 px-0 block" href="#">Treatments</a></li>
          <li><a class="md:p-4 py-3 px-0 block" href="#">Blog</a></li>
          <li><a class="md:p-4 py-3 px-0 block md:mb-0 mb-2" href="#">Contact Us</a></li>
        </ul>
      </nav>
    </div>
    </div>
  </header>
