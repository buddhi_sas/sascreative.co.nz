
<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package sas_Creative
 */

get_header();
?>
<div class="container mx-auto px-4">
<div class="flex flex-wrap text-white gap-4 ">
<div class="w-2/5 border-white border p-5 m-5 rounded shadow">
<h3 class="text-5xl" >Design</h2>
<p>Lorem Ipsum is simply dummy text printing and typesetting printing and typesetting of the typesetting industry.</p>
</div>
<div class="w-2/5 border-white border p-5 m-5 rounded shadow">
<h3 class="text-5xl">Design</h2>
<p>Lorem Ipsum is simply dummy text printing and typesetting printing and typesetting of the typesetting industry.</p>
</div>
<div class="w-2/5 border-white border p-5 m-5 rounded shadow">
<h3 class="text-5xl" >Design</h2>
<p>Lorem Ipsum is simply dummy text printing and typesetting printing and typesetting of the typesetting industry.</p>
</div>
<div class="w-2/5 border-white border p-5 m-5 rounded shadow">
<h3 class="text-5xl">Design</h2>
<p>Lorem Ipsum is simply dummy text printing and typesetting printing and typesetting of the typesetting industry.</p>
</div>
</div>
</div>
<?php
get_sidebar();


